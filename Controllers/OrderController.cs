﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Dynamic;
using TestCandidate.Context;
using TestCandidate.Models;

namespace TestCandidate.Controllers
{
    public class OrderController : Controller
    {
        private readonly ApplicationDbContext _dBContext;

        public OrderController(ApplicationDbContext dBContext)
        {
            _dBContext = dBContext;
        }

        #region Order List
        public IActionResult Index(string param)
        {
            var dataOrder = (from o in _dBContext.Orders
                             join c in _dBContext.Customers on o.CustomerId equals c.CustomerId
                             join od in _dBContext.OrderDetails on o.OrderId equals od.OrderId
                             join p in _dBContext.Products on od.ProductId equals p.ProductId
                             join a in _dBContext.Approvers on o.OrderId equals a.ApproverId
                             group new { o, c, od, p, a } by new
                             {
                                 o.OrderNumber,
                                 o.OrderDate,
                                 c.ContactName,
                                 //c.CustomerType,
                                 o.ShippedDate,
                                 o.ShipName,
                                 c.Phone,
                                 od.UnitPrice,
                                 od.Quantity,
                                 od.Discount,
                                 a.StatusApprove,
                                 a.ApproverName,
                                 //Total = (od.UnitPrice * od.Quantity) - (od.UnitPrice * od.Quantity * (decimal)od.Discount)
                                 Total = od.Quantity * od.UnitPrice
                             } into g
                             where EF.Functions.Like(g.Key.ApproverName, "%" + param + "%")
                             select new
                             {
                                 g.Key.OrderNumber,
                                 g.Key.OrderDate,
                                 g.Key.ContactName,
                                 //g.Key.CustomerType,
                                 g.Key.ShippedDate,
                                 g.Key.ShipName,
                                 g.Key.Phone,
                                 g.Key.UnitPrice,
                                 g.Key.Quantity,
                                 g.Key.Discount,
                                 g.Key.StatusApprove,
                                 g.Key.ApproverName,
                                 g.Key.Total
                             }
                            )
                            .GroupBy(e => new { e.OrderNumber, e.OrderDate, e.ContactName, e.ShippedDate, e.ShipName, e.Phone, /*e.CustomerType,*/ e.StatusApprove, e.ApproverName })
                            .Select(
                                s => new OrderViewModel
                                {
                                    OrderNumber = s.Key.OrderNumber,
                                    OrderDate = s.Key.OrderDate,
                                    ContactName = s.Key.ContactName,
                                    //CustomerType = s.Key.CustomerType,
                                    ShippedDate = s.Key.ShippedDate,
                                    ShipName = s.Key.ShipName,
                                    Phone = s.Key.Phone,
                                    StatusApprove = s.Key.StatusApprove,
                                    ApproverName = s.Key.ApproverName,
                                    SumTotal = (decimal)s.Sum(s => s.Total)
                                }
                            )
                            .ToList();

            return View(dataOrder);
        }
        #endregion

        #region Order Detail
        public ActionResult OrderDetail(string? orderNumber)
        {

            var orderInformation = (from o in _dBContext.Orders
                                    join od in _dBContext.OrderDetails on o.OrderId equals od.OrderId
                                    group new { o, od } by new
                                    {
                                        o.OrderNumber,
                                        o.OrderId,
                                        o.OrderDate,
                                        o.RequiredDate,
                                        //o.TermOfPayment,
                                        od.Quantity,
                                        od.UnitPrice,
                                        SumTotal = od.Quantity * od.UnitPrice
                                    } into g
                                    select new
                                    {
                                        g.Key.OrderNumber,
                                        g.Key.OrderDate,
                                        g.Key.RequiredDate,
                                        //g.Key.TermOfPayment,
                                        g.Key.SumTotal
                                    }
                            )
                            .Where(x => x.OrderNumber == orderNumber)
                            .GroupBy(e => new { e.OrderNumber, e.OrderDate, e.RequiredDate,/* e.TermOfPayment*/ })
                            .Select(
                                s => new OrderViewModel
                                {
                                    OrderNumber = s.Key.OrderNumber,
                                    OrderDate = s.Key.OrderDate,
                                    RequiredDate = s.Key.RequiredDate,
                                    //TermOfPayment = s.Key.TermOfPayment,
                                    Total = (decimal)s.Sum(s => s.SumTotal)
                                }
                            )
                            .ToList();

            var customerInformation = (
                from c in _dBContext.Customers
                join o in _dBContext.Orders on c.CustomerId equals o.CustomerId
                select new
                {
                    o.OrderNumber,
                    c.ContactTitle,
                    c.Phone,
                    c.CustomerId,
                    o.ShippedDate,
                    o.Freight,
                    c.ContactName,
                    c.Address,
                    c.City,
                    c.Region,
                    c.PostalCode,
                    c.Country
                    //o.Notes

                })
                .Where(x => x.OrderNumber == orderNumber)
                .ToList();

            var orderDetailInformation = (
                        from od in _dBContext.OrderDetails
                        join p in _dBContext.Products on od.ProductId equals p.ProductId
                        join o in _dBContext.Orders on od.OrderId equals o.OrderId
                        select new
                        {
                            o.OrderNumber,
                            p.ProductName,
                            od.UnitPrice,
                            od.Quantity,
                            //od.Discount,
                            Total = od.Quantity * od.UnitPrice

                        })
                        .Where(x => x.OrderNumber == orderNumber)
                        .ToList();

            var getOrderId = _dBContext.Orders.Where(x => x.OrderNumber == orderNumber).FirstOrDefault();
            var statusApprove = (from a in _dBContext.Approvers
                                 join o in _dBContext.Orders on a.ApproverId equals o.OrderId
                                 select new
                                 {
                                     o.OrderId,
                                     a.ApproverId,
                                     a.StatusApprove,
                                     a.ApproverName
                                 }
                                )
                                .Where(x => x.ApproverId == getOrderId.OrderId).ToList();


            ViewBag.OrderInfo = orderInformation;
            ViewBag.CustInfo = customerInformation;
            ViewBag.OrderDetailInfo = orderDetailInformation;
            ViewBag.ApprovalStatus = statusApprove;
            return View();
        }
        #endregion

        #region Order Add
        public ActionResult OrderAdd()
        {

            ViewBag.SerialNumber = GetRunningNumber();
            ViewBag.Product = GetProduct();

            return View();
        }
        #endregion

        #region Get Data For Add Form
        public int? GetRunningNumber()
        {
            var dataRunningNumbers = _dBContext.RunningNumbers
                                    .Select(x => x.CurrentNo)
                                    .OrderByDescending(x => x)
                                    .FirstOrDefault();

            int? newSerial = dataRunningNumbers + 1;
            return newSerial;
        }

        public List<Product> GetProduct()
        {
            var dataProducts = _dBContext.Products.OrderBy(a => a.ProductName).ToList();

            return dataProducts;
        }
        #endregion

        #region Add Order
        public IActionResult AddOrder(OrderViewModel data)
        {
            Order order = new Order();
            OrderDetail orderDetail = new OrderDetail();
            RunningNumber runningNumber = new RunningNumber();
            Customer customer = new Customer();
            Approver approver = new Approver();

            //using var transaction = _dBContext.Database.BeginTransaction();
            try
            {
                // Running Number
                int year = _dBContext.RunningNumbers.Max(r => r.Year);
                int month = _dBContext.RunningNumbers.Max(r => r.RunningMonth);
                runningNumber.CurrentNo = data.CurrentNo;
                runningNumber.Year = year + 1;
                runningNumber.RunningMonth = month + 1;
                runningNumber.Prefix = "ORDR";
                _dBContext.Add(runningNumber);
                _dBContext.SaveChanges();

                // Customer
                customer.CustomerId = System.Guid.NewGuid().ToString().Substring(0,5);
                //customer.CustomerType = data.CustomerType;
                customer.CompanyName = data.ContactName;
                customer.ContactName = data.ContactName;
                customer.Phone = data.Phone;
                customer.Address = data.Address;
                //customer.Bank = data.Bank;
                _dBContext.Add(customer);
                _dBContext.SaveChanges();

                // Order
                //order.OrderId = runningNumber.CurrentNo;
                order.OrderNumber = runningNumber.Prefix + '/' + runningNumber.Year.ToString() + '/' + runningNumber.RunningMonth.ToString() + '/' + runningNumber.CurrentNo;
                order.CustomerId = customer.CustomerId;
                order.OrderDate = data.OrderDate;
                order.RequiredDate = data.TermOfPayment;
                order.ShippedDate = data.ShippedDate;
                //order.Notes = data.Notes;
                //order.TermOfPayment = data.RequiredDate;
                _dBContext.Add(order);
                _dBContext.SaveChanges();

                // Order Detail
                var priceByProduct = _dBContext.Products.Where(x => x.ProductId == data.ProductId)
                               .Select(x => new Product() { UnitPrice = x.UnitPrice }).FirstOrDefault();

                orderDetail.OrderId = order.OrderId;
                orderDetail.ProductId = data.ProductId;
                orderDetail.UnitPrice = priceByProduct.UnitPrice;
                orderDetail.Quantity = data.Quantity;
                orderDetail.Discount = (decimal?)0.1;
                decimal? total = orderDetail.Quantity * orderDetail.UnitPrice;
                decimal? tax = total * 10 / 100;
                //orderDetail.GrandTotal = total + tax;
                _dBContext.Add(orderDetail);
                _dBContext.SaveChanges();

                // Approver
                approver.ApproverId = order.OrderId;
                approver.ApproverName = "";
                approver.StatusApprove = false;
                _dBContext.Add(approver);
                _dBContext.SaveChanges();

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
                //transaction.Rollback();
            }

            return RedirectToAction("Index");
        }
        #endregion

        #region Edit Order
        public IActionResult EditOrder(OrderViewModel data)
        {
            Order order = new Order();
            OrderDetail orderDetail = new OrderDetail();
            Customer customer = new Customer();

            var orderData = _dBContext.Orders.Where(x => x.OrderNumber == data.OrderNumber).FirstOrDefault();
            orderData.OrderDate = data.OrderDate;
            orderData.RequiredDate = data.RequiredDate;
            orderData.ShippedDate = data.ShippedDate;
            //orderData.TermOfPayment = data.TermOfPayment;
            orderData.ShipName = data.ShipName;
            orderData.ShippedDate = data.ShippedDate;
            orderData.Freight = data.Freight;
            orderData.ShipAddress = data.Address;
            orderData.ShipCity = data.City;
            orderData.ShipRegion = data.Region;
            orderData.ShipPostalCode = data.PostalCode;
            orderData.ShipCountry = data.Country;
            //orderData.Notes = data.Notes;
            _dBContext.SaveChanges();

            var customerData = _dBContext.Customers.Where(x => x.CustomerId == data.CustomerId).FirstOrDefault();
            customerData.CompanyName = customerData.CompanyName;
            customerData.ContactName = data.ContactName;
            customerData.Address = data.Address;
            customerData.City = data.City;
            customerData.Region = data.Region;
            customerData.PostalCode = data.PostalCode;
            customerData.Country = data.Country;
            customerData.Phone = data.Phone;
            customerData.Fax = data.Fax;
            //customerData.Bank = data.Bank;

            _dBContext.SaveChanges();


            return RedirectToAction("Index");
        }

        #endregion

        #region Delete
        public IActionResult DeleteOrder(string orderNumber)
        {
            Order order = new Order();
            OrderDetail orderDetail = new OrderDetail();
            RunningNumber runningNumber = new RunningNumber();
            Approver update = new Approver();

            var orderData = _dBContext.Orders.Where(x => x.OrderNumber == orderNumber).FirstOrDefault();
            var orderDataDetail = _dBContext.OrderDetails.Where(x => x.OrderId == orderData.OrderId).FirstOrDefault();
            var dataRunning = _dBContext.RunningNumbers.Where(x => x.CurrentNo == orderData.OrderId).FirstOrDefault();
            var approverData = _dBContext.Approvers.Where(x => x.ApproverId == orderData.OrderId).FirstOrDefault();

            _dBContext.RemoveRange(dataRunning);
            _dBContext.SaveChanges();

            _dBContext.RemoveRange(orderDataDetail);
            _dBContext.RemoveRange(orderData);
            _dBContext.RemoveRange(approverData);

            return RedirectToAction("Index");
        }
        #endregion

        #region Approve
        public IActionResult ApproverUpdate(int? approverId, string? approverName)
        {
            Approver update = new Approver();
            var approverData = _dBContext.Approvers.Where(x => x.ApproverId == approverId).FirstOrDefault();
            approverData.ApproverName = approverName;
            approverData.StatusApprove = true;

            _dBContext.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion
    }
}
