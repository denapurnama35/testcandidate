﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Linq;
using TestCandidate.Context;
using TestCandidate.Models;
using static TestCandidate.Models.Chart;

namespace TestCandidate.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _dBContext;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext dBContext)
        {
            _logger = logger;
            _dBContext = dBContext;
        }

        public IActionResult Index()
        {
            var dataOrder = (from o in _dBContext.Orders
                             //join c in _dBContext.Customers on o.CustomerId equals c.CustomerId
                             join od in _dBContext.OrderDetails on o.OrderId equals od.OrderId
                             join p in _dBContext.Products on od.ProductId equals p.ProductId
                             group new { o, /*c,*/ od, p} by new
                             {
                                 o.OrderDate,
                                 od.UnitPrice,
                                 od.Quantity,
                                 //p.ProductName,
                                 Total = od.Quantity * od.UnitPrice
                             } into g

                             select new
                             {
                                 g.Key.OrderDate,
                                 g.Key.UnitPrice,
                                 g.Key.Quantity,
                                 //g.Key.ProductName,
                                 g.Key.Total
                             }
                )
                .GroupBy(e => new { e.OrderDate/*, e.ProductName*/ })
                .Select(
                    s => new OrderViewModel
                    {
                        OrderDates = s.Key.OrderDate.ToString().Substring(7,4),
                        //ProductName = s.Key.ProductName,
                        SumTotal = (decimal)s.Sum(s => s.Total)
                    }
                )
                .ToList();

            var dataProduct = _dBContext.OrderDetails
                            .GroupBy(p => p.OrderId)
                            .Select(g => new
                            {
                                OrderId = g.Key,
                                Count = g.Count()
                            })
                             .ToList();

            ViewBag.DataPoints = dataOrder;
            ViewBag.DataProduct = dataProduct;


            return View();
        }
    }
}