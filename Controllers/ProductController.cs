﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestCandidate.Context;
using TestCandidate.Models;

namespace TestCandidate.Controllers
{
    public class ProductController : Controller
    {
        private readonly ApplicationDbContext _dBContext;

        public ProductController(ApplicationDbContext dBContext)
        {
            _dBContext = dBContext;
        }
        public IActionResult Index()
        {
            var products = _dBContext.Products.OrderBy(a => a.ProductName).ToList();

            return View(products);
        }

        #region AddProduct
        public IActionResult AddProduct(Product data)
        {
            var dataProduct = _dBContext.Products.Where(x => x.ProductName == data.ProductName).Select(x => new Product()
            {
                ProductName = x.ProductName
            }).FirstOrDefault();

            if (dataProduct != null)
            {
                return Json(new { message = "Product with name: " + dataProduct.ProductName + " Already Exist" });
            }

            _dBContext.Add(data);
            _dBContext.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion


        #region UpdateProduct
        public IActionResult UpdateProduct(Product data)
        {
            _dBContext.Update(data);
            _dBContext.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Delete
        public IActionResult DeleteProduct(Product data)
        {
            _dBContext.Remove(data);
            _dBContext.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion


    }
}
