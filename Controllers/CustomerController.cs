﻿using Microsoft.AspNetCore.Mvc;
using TestCandidate.Context;
using TestCandidate.Models;

namespace TestCandidate.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ApplicationDbContext _dBContext;

        public CustomerController(ApplicationDbContext dBContext)
        {
            _dBContext = dBContext;
        }
        public IActionResult Index()
        {
            var customers = _dBContext.Customers.OrderBy(a => a.CustomerId).ToList();

            return View(customers);
        }

        #region AddCustomer
        public IActionResult AddCustomer(Customer data)
        {
            var dataCustomer = _dBContext.Customers.Where(x => x.CompanyName == data.CompanyName).Where(x => x.ContactName == data.ContactName).Select(x => new Customer()
            {
                CompanyName = x.CompanyName,
                ContactName = x.CompanyName
            }).FirstOrDefault();

            if (dataCustomer != null)
            {
                return Json(new { message = "Company name: " + data.CompanyName + " with Contact Name" + data.ContactName + " Already Exist" });
            }
            //Random rnd = new Random();
            //data.CustomerId = (char)rnd.Next('1', '9');

            _dBContext.Add(data);
            _dBContext.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion


        #region UpdateCustomer
        public IActionResult UpdateCustomer(Customer data)
        {
            _dBContext.Update(data);
            _dBContext.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion

        #region Delete
        public IActionResult DeleteCustomer(Customer data)
        {
            _dBContext.Remove(data);
            _dBContext.SaveChanges();
            return RedirectToAction("Index");
        }
        #endregion
    }
}
