USE [master]
GO
/****** Object:  Database [TestCandidate]    Script Date: 11/10/2022 16:48:14 ******/
CREATE DATABASE [TestCandidate]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TestCandidate', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\TestCandidate.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'TestCandidate_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\TestCandidate_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [TestCandidate] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TestCandidate].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TestCandidate] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TestCandidate] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TestCandidate] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TestCandidate] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TestCandidate] SET ARITHABORT OFF 
GO
ALTER DATABASE [TestCandidate] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TestCandidate] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TestCandidate] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TestCandidate] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TestCandidate] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TestCandidate] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TestCandidate] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TestCandidate] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TestCandidate] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TestCandidate] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TestCandidate] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TestCandidate] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TestCandidate] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TestCandidate] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TestCandidate] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TestCandidate] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TestCandidate] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TestCandidate] SET RECOVERY FULL 
GO
ALTER DATABASE [TestCandidate] SET  MULTI_USER 
GO
ALTER DATABASE [TestCandidate] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TestCandidate] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TestCandidate] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TestCandidate] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [TestCandidate] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [TestCandidate] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'TestCandidate', N'ON'
GO
ALTER DATABASE [TestCandidate] SET QUERY_STORE = OFF
GO
USE [TestCandidate]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 11/10/2022 16:48:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Approver]    Script Date: 11/10/2022 16:48:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Approver](
	[ApproverId] [int] NOT NULL,
	[ApproverName] [varchar](50) NOT NULL,
	[StatusApprove] [bit] NOT NULL,
 CONSTRAINT [PK_Approver] PRIMARY KEY CLUSTERED 
(
	[ApproverId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 11/10/2022 16:48:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[CustomerID] [nchar](5) NOT NULL,
	[CompanyName] [nvarchar](40) NOT NULL,
	[ContactName] [nvarchar](30) NULL,
	[ContactTitle] [nvarchar](30) NULL,
	[Address] [nvarchar](60) NULL,
	[City] [nvarchar](15) NULL,
	[Region] [nvarchar](15) NULL,
	[PostalCode] [nvarchar](10) NULL,
	[Country] [nvarchar](15) NULL,
	[Phone] [nvarchar](24) NULL,
	[Fax] [nvarchar](24) NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order Details]    Script Date: 11/10/2022 16:48:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order Details](
	[OrderID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[UnitPrice] [money] NOT NULL,
	[Quantity] [smallint] NOT NULL,
	[Discount] [real] NOT NULL,
 CONSTRAINT [PK_Order_Details] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC,
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 11/10/2022 16:48:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[OrderNumber] [nvarchar](50) NOT NULL,
	[CustomerID] [nchar](5) NULL,
	[OrderDate] [datetime] NULL,
	[RequiredDate] [datetime] NULL,
	[ShippedDate] [datetime] NULL,
	[Freight] [money] NULL,
	[ShipName] [nvarchar](40) NULL,
	[ShipAddress] [nvarchar](60) NULL,
	[ShipCity] [nvarchar](15) NULL,
	[ShipRegion] [nvarchar](15) NULL,
	[ShipPostalCode] [nvarchar](10) NULL,
	[ShipCountry] [nvarchar](15) NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 11/10/2022 16:48:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](40) NOT NULL,
	[QuantityPerUnit] [nvarchar](20) NULL,
	[UnitPrice] [money] NULL,
	[UnitsInStock] [smallint] NULL,
	[UnitsOnOrder] [smallint] NULL,
	[ReorderLevel] [smallint] NULL,
	[Discontinued] [bit] NOT NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RunningNumber]    Script Date: 11/10/2022 16:48:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RunningNumber](
	[Year] [int] NOT NULL,
	[RunningMonth] [int] NOT NULL,
	[Prefix] [varchar](10) NULL,
	[CurrentNo] [int] NULL,
 CONSTRAINT [PK_RunningNumber] PRIMARY KEY CLUSTERED 
(
	[Year] ASC,
	[RunningMonth] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Approver] ([ApproverId], [ApproverName], [StatusApprove]) VALUES (1, N'Dena', 1)
INSERT [dbo].[Approver] ([ApproverId], [ApproverName], [StatusApprove]) VALUES (7, N'Wahyudi', 1)
INSERT [dbo].[Approver] ([ApproverId], [ApproverName], [StatusApprove]) VALUES (8, N'', 0)
INSERT [dbo].[Approver] ([ApproverId], [ApproverName], [StatusApprove]) VALUES (9, N'', 0)
INSERT [dbo].[Approver] ([ApproverId], [ApproverName], [StatusApprove]) VALUES (10, N'', 0)
GO
INSERT [dbo].[Customers] ([CustomerID], [CompanyName], [ContactName], [ContactTitle], [Address], [City], [Region], [PostalCode], [Country], [Phone], [Fax]) VALUES (N'3241d', N'Test Lagi', N'Test Lagi', NULL, N'Test Lagi', NULL, NULL, NULL, N'India', N'123', NULL)
INSERT [dbo].[Customers] ([CustomerID], [CompanyName], [ContactName], [ContactTitle], [Address], [City], [Region], [PostalCode], [Country], [Phone], [Fax]) VALUES (N'3ae6f', N'Hariono', N'Hariono', NULL, N'Bandung', NULL, NULL, NULL, NULL, N'314', NULL)
INSERT [dbo].[Customers] ([CustomerID], [CompanyName], [ContactName], [ContactTitle], [Address], [City], [Region], [PostalCode], [Country], [Phone], [Fax]) VALUES (N'5f896', N'Dede', N'Dede', NULL, N'Jalan jalan dulu', NULL, NULL, NULL, N'Indonesia', N'2412', NULL)
INSERT [dbo].[Customers] ([CustomerID], [CompanyName], [ContactName], [ContactTitle], [Address], [City], [Region], [PostalCode], [Country], [Phone], [Fax]) VALUES (N'dfc65', N'test', N'Dena Lagi', NULL, N'test', NULL, NULL, NULL, NULL, N'1213', NULL)
INSERT [dbo].[Customers] ([CustomerID], [CompanyName], [ContactName], [ContactTitle], [Address], [City], [Region], [PostalCode], [Country], [Phone], [Fax]) VALUES (N'IGLO ', N'IGLO', N'Iglo', N'Sales Representative', N'Jakarta', N'Jakarta', N'DKI', N'123123', N'Indonesia', N'839210', N'02391')
GO
INSERT [dbo].[Order Details] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Discount]) VALUES (1, 1, 200.0000, 100, 0)
INSERT [dbo].[Order Details] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Discount]) VALUES (7, 1, 20000.0000, 1, 0)
INSERT [dbo].[Order Details] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Discount]) VALUES (7, 2, 200.0000, 1, 0.1)
INSERT [dbo].[Order Details] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Discount]) VALUES (7, 3, 1000.0000, 1, 0)
INSERT [dbo].[Order Details] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Discount]) VALUES (8, 3, 10.0000, 10000, 0.1)
INSERT [dbo].[Order Details] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Discount]) VALUES (9, 3, 10.0000, 650, 0.1)
INSERT [dbo].[Order Details] ([OrderID], [ProductID], [UnitPrice], [Quantity], [Discount]) VALUES (10, 2, 100.0000, 13, 0.1)
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([OrderID], [OrderNumber], [CustomerID], [OrderDate], [RequiredDate], [ShippedDate], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (1, N'ODR/18/08/0001', N'IGLO ', CAST(N'2018-08-14T00:00:00.000' AS DateTime), CAST(N'2018-08-20T00:00:00.000' AS DateTime), CAST(N'2018-08-17T00:00:00.000' AS DateTime), 314.0000, N'Iglo', N'Jakarta', N'Jakarta', N'DKI', N'123123', N'Indonesia')
INSERT [dbo].[Orders] ([OrderID], [OrderNumber], [CustomerID], [OrderDate], [RequiredDate], [ShippedDate], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (7, N'ORDR/2022/10/2', N'dfc65', CAST(N'2022-10-10T00:00:00.000' AS DateTime), CAST(N'2022-10-11T00:00:00.000' AS DateTime), CAST(N'2022-10-11T00:00:00.000' AS DateTime), NULL, NULL, N'test', NULL, NULL, NULL, NULL)
INSERT [dbo].[Orders] ([OrderID], [OrderNumber], [CustomerID], [OrderDate], [RequiredDate], [ShippedDate], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (8, N'ORDR/2023/11/3', N'3241d', CAST(N'2020-10-11T00:00:00.000' AS DateTime), CAST(N'2020-10-11T00:00:00.000' AS DateTime), CAST(N'2020-10-11T00:00:00.000' AS DateTime), NULL, NULL, N'Test Lagi', NULL, NULL, NULL, N'India')
INSERT [dbo].[Orders] ([OrderID], [OrderNumber], [CustomerID], [OrderDate], [RequiredDate], [ShippedDate], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (9, N'ORDR/2024/12/4', N'5f896', CAST(N'2019-10-11T00:00:00.000' AS DateTime), CAST(N'2019-10-31T00:00:00.000' AS DateTime), CAST(N'2019-10-18T00:00:00.000' AS DateTime), NULL, NULL, N'Jalan jalan dulu', NULL, NULL, NULL, N'Indonesia')
INSERT [dbo].[Orders] ([OrderID], [OrderNumber], [CustomerID], [OrderDate], [RequiredDate], [ShippedDate], [Freight], [ShipName], [ShipAddress], [ShipCity], [ShipRegion], [ShipPostalCode], [ShipCountry]) VALUES (10, N'ORDR/2025/13/5', N'3ae6f', CAST(N'2017-10-12T00:00:00.000' AS DateTime), CAST(N'2017-10-25T00:00:00.000' AS DateTime), CAST(N'2017-10-18T00:00:00.000' AS DateTime), 0.0000, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([ProductID], [ProductName], [QuantityPerUnit], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [ReorderLevel], [Discontinued]) VALUES (1, N'Mac Pro', N'Unit', 200.0000, 1000, 100, 1, 0)
INSERT [dbo].[Products] ([ProductID], [ProductName], [QuantityPerUnit], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [ReorderLevel], [Discontinued]) VALUES (2, N'Iphone', N'Unit', 100.0000, 490, 100, 1, 0)
INSERT [dbo].[Products] ([ProductID], [ProductName], [QuantityPerUnit], [UnitPrice], [UnitsInStock], [UnitsOnOrder], [ReorderLevel], [Discontinued]) VALUES (3, N'Ayam', N'Ekor', 10.0000, 1, 1, 1, 0)
SET IDENTITY_INSERT [dbo].[Products] OFF
GO
INSERT [dbo].[RunningNumber] ([Year], [RunningMonth], [Prefix], [CurrentNo]) VALUES (2018, 8, N'ODR', 1)
INSERT [dbo].[RunningNumber] ([Year], [RunningMonth], [Prefix], [CurrentNo]) VALUES (2022, 10, N'ORDR', 2)
INSERT [dbo].[RunningNumber] ([Year], [RunningMonth], [Prefix], [CurrentNo]) VALUES (2023, 11, N'ORDR', 3)
INSERT [dbo].[RunningNumber] ([Year], [RunningMonth], [Prefix], [CurrentNo]) VALUES (2024, 12, N'ORDR', 4)
INSERT [dbo].[RunningNumber] ([Year], [RunningMonth], [Prefix], [CurrentNo]) VALUES (2025, 13, N'ORDR', 5)
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Orders]    Script Date: 11/10/2022 16:48:14 ******/
ALTER TABLE [dbo].[Orders] ADD  CONSTRAINT [IX_Orders] UNIQUE NONCLUSTERED 
(
	[OrderNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Order Details] ADD  CONSTRAINT [DF_Order_Details_UnitPrice]  DEFAULT ((0)) FOR [UnitPrice]
GO
ALTER TABLE [dbo].[Order Details] ADD  CONSTRAINT [DF_Order_Details_Quantity]  DEFAULT ((1)) FOR [Quantity]
GO
ALTER TABLE [dbo].[Order Details] ADD  CONSTRAINT [DF_Order_Details_Discount]  DEFAULT ((0)) FOR [Discount]
GO
ALTER TABLE [dbo].[Orders] ADD  CONSTRAINT [DF_Orders_Freight]  DEFAULT ((0)) FOR [Freight]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Products_UnitPrice]  DEFAULT ((0)) FOR [UnitPrice]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Products_UnitsInStock]  DEFAULT ((0)) FOR [UnitsInStock]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Products_UnitsOnOrder]  DEFAULT ((0)) FOR [UnitsOnOrder]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Products_ReorderLevel]  DEFAULT ((0)) FOR [ReorderLevel]
GO
ALTER TABLE [dbo].[Products] ADD  CONSTRAINT [DF_Products_Discontinued]  DEFAULT ((0)) FOR [Discontinued]
GO
ALTER TABLE [dbo].[Order Details]  WITH NOCHECK ADD  CONSTRAINT [FK_Order_Details_Orders] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Orders] ([OrderID])
GO
ALTER TABLE [dbo].[Order Details] CHECK CONSTRAINT [FK_Order_Details_Orders]
GO
ALTER TABLE [dbo].[Order Details]  WITH NOCHECK ADD  CONSTRAINT [FK_Order_Details_Products] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Products] ([ProductID])
GO
ALTER TABLE [dbo].[Order Details] CHECK CONSTRAINT [FK_Order_Details_Products]
GO
ALTER TABLE [dbo].[Orders]  WITH NOCHECK ADD  CONSTRAINT [FK_Orders_Customers] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Customers]
GO
ALTER TABLE [dbo].[Order Details]  WITH NOCHECK ADD  CONSTRAINT [CK_Discount] CHECK  (([Discount]>=(0) AND [Discount]<=(1)))
GO
ALTER TABLE [dbo].[Order Details] CHECK CONSTRAINT [CK_Discount]
GO
ALTER TABLE [dbo].[Order Details]  WITH NOCHECK ADD  CONSTRAINT [CK_Quantity] CHECK  (([Quantity]>(0)))
GO
ALTER TABLE [dbo].[Order Details] CHECK CONSTRAINT [CK_Quantity]
GO
ALTER TABLE [dbo].[Order Details]  WITH NOCHECK ADD  CONSTRAINT [CK_UnitPrice] CHECK  (([UnitPrice]>=(0)))
GO
ALTER TABLE [dbo].[Order Details] CHECK CONSTRAINT [CK_UnitPrice]
GO
ALTER TABLE [dbo].[Products]  WITH NOCHECK ADD  CONSTRAINT [CK_Products_UnitPrice] CHECK  (([UnitPrice]>=(0)))
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [CK_Products_UnitPrice]
GO
ALTER TABLE [dbo].[Products]  WITH NOCHECK ADD  CONSTRAINT [CK_ReorderLevel] CHECK  (([ReorderLevel]>=(0)))
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [CK_ReorderLevel]
GO
ALTER TABLE [dbo].[Products]  WITH NOCHECK ADD  CONSTRAINT [CK_UnitsInStock] CHECK  (([UnitsInStock]>=(0)))
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [CK_UnitsInStock]
GO
ALTER TABLE [dbo].[Products]  WITH NOCHECK ADD  CONSTRAINT [CK_UnitsOnOrder] CHECK  (([UnitsOnOrder]>=(0)))
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [CK_UnitsOnOrder]
GO
USE [master]
GO
ALTER DATABASE [TestCandidate] SET  READ_WRITE 
GO
