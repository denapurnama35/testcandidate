﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestCandidate.Models
{
    public class Order
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int? OrderId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string OrderNumber { get; set; } = null!;
        public string? CustomerId { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public DateTime? ShippedDate { get; set; }
        //public DateTime? TermOfPayment { get; set; }
        public decimal? Freight { get; set; }
        public string? ShipName { get; set; }
        public string? ShipAddress { get; set; }
        public string? ShipCity { get; set; }
        public string? ShipRegion { get; set; }
        public string? ShipPostalCode { get; set; }
        public string? ShipCountry { get; set; }
        //public string? Notes { get; set; }

        public virtual Customer? Customer { get; set; }
        //public virtual RunningNumber? RunningNumbers { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
