﻿namespace TestCandidate.Models
{
    public class Approver
    {
        public int? ApproverId { get; set; }
        public string? ApproverName { get; set; }
        public bool StatusApprove { get; set; }
    }
}
