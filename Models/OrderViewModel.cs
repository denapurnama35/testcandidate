﻿namespace TestCandidate.Models
{
    public class OrderViewModel
    {
        public string OrderNumber { get; set; } = null!;
        public DateTime? OrderDate { get; set; }
        public string? OrderDates { get; set; }
        public DateTime? ShippedDate { get; set; } 
        public DateTime? RequiredDate { get; set; }
        public DateTime? TermOfPayment { get; set; }
        public TimeSpan? DateDiff { get; set; }
        public string? ShipName { get; set; }
        //public string? Notes { get; set; }
        public decimal SumTotal { get; set; }
        public decimal Total { get; set; }
        public short Quantity { get; set; }
        public decimal? Discount { get; set; }
        public bool StatusApprove { get; set; }
        public int? ApproverId{ get; set; }
        public string? ApproverName { get; set; }


        // Product
        public int ProductId { get; set; }
        public string ProductName { get; set; } = null!;
        public string? QuantityPerUnit { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? Freight { get; set; }
        public short? UnitsInStock { get; set; }
        public short? UnitsOnOrder { get; set; }
        public short? ReorderLevel { get; set; }
        public bool Discontinued { get; set; }

        // Running Number
        public int Year { get; set; }
        public int RunningMonth { get; set; }
        public string? Prefix { get; set; }
        public int? CurrentNo { get; set; }

        // Customer
        public string? CustomerId { get; set; }
        public string CompanyName { get; set; } = null!;
        public string? CustomerType { get; set; }
        public string? Bank { get; set; }
        public string? ContactName { get; set; }
        public string? ContactTitle { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? Region { get; set; }
        public string? PostalCode { get; set; }
        public string? Country { get; set; }
        public string? Phone { get; set; }
        public string? Fax { get; set; }

    }
}
